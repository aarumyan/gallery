<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="exhibition-content">
    <div class="collections-items-wrap masonry ajax_content_more" id="collections-items-wrap">
        <div class="collections-item-gutter-sizer"></div>
        <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?><br />
        <? endif; ?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="collections-item content_ajax_item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="show-slider__item">
                    <div class="show-main-item">
                        <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                            <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                                <a class="show-img-link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                    <span class="show-img-wrap">
                                        <img
                                            class="preview_picture"
                                            src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                            alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                            title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"                       
                                            />
                                    </span>
                                </a>
                            <? else: ?>
                                <span class="show-img-wrap">
                                    <img
                                        class="preview_picture"
                                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                        alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                        title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                                        />
                                </span>
                            <? endif; ?>
                        <? endif ?> 

                        <? if ($arItem["DATE_ACTIVE_FROM"] && $arItem['DATE_ACTIVE_TO']): ?>
                            <span class="show-main-item__date"><?= $arItem["DATE_ACTIVE_FROM"] ?> - <?= $arItem['DATE_ACTIVE_TO'] ?></span>
                            <? else: ?>
                             <span class="show-main-item__date"><?= $arItem["DATE_ACTIVE_FROM"] ?> <?= $arItem['DATE_ACTIVE_TO'] ?></span>
                        <? endif ?>
                        <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                            <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                                <h5 class="show-main-item__title">
                                    <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                                </h5>
                            <? else: ?>
                                <h5 class="show-main-item__title"><? echo $arItem["NAME"] ?></h5>
                            <? endif; ?>
                        <? endif; ?>
                        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                            <div class="show-main-item__text"><?= TruncateText($arItem["PREVIEW_TEXT"], 141) ?></div>
                        <? endif; ?>
                        <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                            <div style="clear:both"></div>
                        <? endif ?>
                        <div class="location-item show-main-item__location">
                            <span class="location-item__ic"></span>
                             <a class="location-item__text" href="/filials/<?= $arItem['PROPERTIES']['PLACE']['VALUE']?>/">
                            <? 
                                $name = CIBlockElement::GetByID($arItem['PROPERTIES']['PLACE']['VALUE']);
                                if($name = $name->GetNext())
                                    echo $name['NAME'];
                            ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <br /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
