!function(e){function n(n){for(var o,a,l=n[0],s=n[1],c=n[2],d=0,f=[];d<l.length;d++)a=l[d],r[a]&&f.push(r[a][0]),r[a]=0;for(o in s)Object.prototype.hasOwnProperty.call(s,o)&&(e[o]=s[o]);for(u&&u(n);f.length;)f.shift()();return i.push.apply(i,c||[]),t()}function t(){for(var e,n=0;n<i.length;n++){for(var t=i[n],o=!0,l=1;l<t.length;l++){var s=t[l];0!==r[s]&&(o=!1)}o&&(i.splice(n--,1),e=a(a.s=t[0]))}return e}var o={},r={0:0},i=[];function a(n){if(o[n])return o[n].exports;var t=o[n]={i:n,l:!1,exports:{}};return e[n].call(t.exports,t,t.exports,a),t.l=!0,t.exports}a.m=e,a.c=o,a.d=function(e,n,t){a.o(e,n)||Object.defineProperty(e,n,{enumerable:!0,get:t})},a.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},a.t=function(e,n){if(1&n&&(e=a(e)),8&n)return e;if(4&n&&"object"==typeof e&&e&&e.__esModule)return e;var t=Object.create(null);if(a.r(t),Object.defineProperty(t,"default",{enumerable:!0,value:e}),2&n&&"string"!=typeof e)for(var o in e)a.d(t,o,function(n){return e[n]}.bind(null,o));return t},a.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return a.d(n,"a",n),n},a.o=function(e,n){return Object.prototype.hasOwnProperty.call(e,n)},a.p="";var l=window.webpackJsonp=window.webpackJsonp||[],s=l.push.bind(l);l.push=n,l=l.slice();for(var c=0;c<l.length;c++)n(l[c]);var u=s;i.push([288,1]),t()}({288:function(e,n,t){"use strict";t.r(n);t(112);var o=t(111),r=t.n(o),i=t(26),a=t.n(i);t(279),t(280),t(281),t(282);r()(),window.$=a.a,window.jQuery=a.a;t(284),t(285),t(287);$(document).ready(function(){$(".info-block__close").on("click",function(){$(this).parents(".info-block").eq(0).stop().fadeOut()}),$(".slider-fade").on("init",function(e,n){$(this).addClass("slider-init")}),$(".main-slider").slick({autoplay:!0,autoplaySpeed:3e3,slidesToShow:1,slidesToScroll:1,arrows:!0,dots:!1,fade:!0,infinite:!1,appendArrows:$(".main-slider-arrows-wrap")}),

$(".events-slider").each(function(e,n){$(this).slick({slidesToShow:1,slidesToScroll:1,variableWidth:!0,arrows:!0,dots:!1,fade:!1,infinite:!0,appendArrows:$(this).parents(".events-slider-container").eq(0).find(".slider-arrows-wrap")})}),

function(){var e=$("#scroll-to-top");$(window).scrollTop()>$(".header").innerHeight()&&e.addClass("active"),$(window).on("scroll",function(){$(this).scrollTop()>$(".header").innerHeight()?e.addClass("active"):e.removeClass("active")}),e.on("click",function(e){e.preventDefault();var n=$(this);$("body, html").animate({scrollTop:0},{duration:500,easing:"easeOutQuad",complete:function(){n.blur()}})})}(),function(){var e=$("#collections-items-wrap");e.imagesLoaded(function(){e.masonry({columnWidth:".collections-item",gutter:".collections-item-gutter-sizer",itemSelector:".collections-item",percentPosition:!0,initLayout:!1}),e.masonry("on","layoutComplete",function(){e.addClass("collections-items-wrap-init")}),e.masonry()})}()}),$(window).on("load",function(){})}});
//# sourceMappingURL=main.js.map

$(document).ready(function () {
	
	$('#accordion').on('shown.bs.collapse', function (e) {
	  $('.slider-action').slick({
		dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1, 
		fade: true,
	});
	})
	
	$('.slider-filials').slick({
		dots: true,
        infinite: true,
        slidesToShow: 1,
		fade: true,
        slidesToScroll: 1, 
	
	});
	
	$('.gallery-one-slider').slick({
		dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1, 
	
	});
	
	
	$('.slider-other').slick({
		dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1, 
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	
	$('.slider-exibitions').slick({
		dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1, 
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	
	$('.sliderfilial').slick({
		dots: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1, 
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow:1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	
	(function($) {
		$(function() {
			$('select').styler({
				selectSearch: true,
			});
		});
	})(jQuery);

	new UISearch( document.getElementById( 'sb-search' ) );
	
	
	$('.product-slider').slick({
		dots: false,
        infinite: true,
        slidesToShow: 1,
		fade: true,
        slidesToScroll: 1, 
	});
	
	$("#menu").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 1500);
	});
	
	$i = 0; 
	$('.menu-toggle-cont').click(function () { 
	if ($i == 0) { 
	$(this).addClass('menu-toggle-cont_active'); 
	$('.fixed-menu').slideDown('fast'); 
	$i = 1; 
	} else { 
	$(this).removeClass('menu-toggle-cont_active'); 
	$('.fixed-menu').slideUp('fast'); 
	$i = 0; 
	} 
	});
	
	$('.time-menu').each(function () {
		var $list        = $(this),
			$listItemSub = $list.find('.sub-menu');

		$listItemSub.each(function () {
			var $el      = $(this),
			$link    = $el.find('.time-menu__link'),
			$subMenu = $el.find('.time-menu__dropdown');

			$link.on('click', function (e) {
				e.preventDefault();
				$(this).toggleClass('open');
				$subMenu.slideToggle()
			});
		});
	});
	
	$('.nav-razdel[data-toggle=dropdown]').off('click.bs.dropdown').on('click', function (event) { 
	event.preventDefault(); 
	event.stopPropagation(); 
	var preventClass = false; 
	var clickedLink = $(this); 
	if ($(this).closest('li').hasClass('show')) { 
	preventClass = true; 
	} 

	if (!preventClass) { 
	clickedLink.closest('ul').find('li').each(function () { 
	$(this).removeClass('show'); 
	$(this).find('ul').removeClass('show'); 
	}); 
	clickedLink.closest('li').addClass('show'); 
	clickedLink.closest('li').find('ul').addClass('show'); 
	} else { 
	clickedLink.closest('li').removeClass('show'); 
	clickedLink.closest('li').find('ul').removeClass('show'); 
	} 
	});
		
});
$(window).resize(function() {
	if ($(window).width() < 992) { 
		$('header').removeClass("sticky_header");
	}
});
$(window).scroll(function() {
	if ($(window).width() >= 992) { 
		if ($(this).scrollTop() > 1){
		$('header').addClass("sticky_header");
		}
		else{
		$('header').removeClass("sticky_header");
		}
	}
});