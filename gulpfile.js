var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    // ftp = require( 'vinyl-ftp' ),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/images/',
        fonts: 'build/fonts/',
    },
    src: {
        html: 'src/*.html',
        js: [
            'src/js/holmax.js',
            'src/js/script.js',
			'bower_components/masonry-layout/dist/masonry.pkgd.min.js',
            'bower_components/imagesloaded/imagesloaded.pkgd.min.js',
			'bower_components/jquery/dist/jquery.min.js',
        ],
        vendor: [
            'src/js/popper.min.js',
			'src/js/uisearch.js',
			'src/js/classie.js',
			'src/js/jquery.formstyler.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/owl.carousel/dist/owl.carousel.min.js',
            'bower_components/fancybox/dist/jquery.fancybox.js',
            'bower_components/jquery.cookie/jquery.cookie.js',
			'bower_components/wow/dist/wow.min.js',
        ],
        style: 'src/css/**/*.css',
        img: 'src/images/**/*.*',
        fonts: [
            'src/fonts/**/*.*',
            'src/webfonts/**/*.*'
        ],
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/css/**/*.css',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9010,
    logPrefix: "HOLMAX"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    //setTimeout(function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({
            stream: true
        }));
    //},300);
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        //.pipe(concat('holmax.min.js'))
        //.pipe(uglify()) 
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('vendorjs:build', function () {
    gulp.src(path.src.vendor)
        .pipe(rigger())
        //.pipe(sourcemaps.init())
        .pipe(concat('vendor.min.js'))
        .pipe(uglify()) 
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        //.pipe(sourcemaps.init())
        /*.pipe(sass({
            includePaths: ['src/scss/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))*/
        .pipe(prefixer())
        .pipe(cssmin())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        // .pipe(imagemin({
        //     progressive: true,
        //     svgoPlugins: [{removeViewBox: false}],
        //     use: [pngquant()],
        //     interlaced: true
        // }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'vendorjs:build',
    'style:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function () {

    watch([path.watch.html], function (event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function (event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:build');
    });
});

//gulp.task('deploy', function() {
//
//    var conn = ftp.create({
//        host:      'hostname.com',
//        user:      'username',
//        password:  'userpassword',
//        parallel:  10,
//        log: gutil.log
//    });
//
//    var globs = [
//        'build/**',
//        'build/.htaccess'
//    ];
//    return gulp.src(globs, {buffer: false})
//        .pipe(conn.dest('/path/to/folder/on/server'));
//
//});

gulp.task('default', ['build', 'webserver', 'watch']);
