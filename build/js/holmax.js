$.noConflict();

jQuery(document).ready(function ($) {
	
    $('select').styler({
        selectSearch: true,
    });
	
	$('.custom-checkbox input').styler('destroy');
		
	
    new UISearch(document.getElementById('sb-search'));
	
	if($('.history-page_filter').length > 0){
	(function() {
        var a = document.querySelector('#aside'),
            b = null;
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);

        function Ascroll() {
            if (b == null) {
                var Sa = getComputedStyle(a, ''),
                    s = '';
                for (var i = 0; i < Sa.length; i++) {
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b = document.createElement('div');
                b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                a.insertBefore(b, a.firstChild);
                var l = a.childNodes.length;
                for (var i = 1; i < l; i++) {
                    b.appendChild(a.childNodes[1]);
                }
                a.style.height = b.getBoundingClientRect().height + 'px'; // если под скользящим элементом есть другие блоки, можно своё значение
                a.style.padding = '0';
                a.style.border = '0';
            }
            if (a.getBoundingClientRect().top <= 0) {
                b.className = 'sticky';
            } else {
                b.className = '';
            }
            window.addEventListener('resize', function() {
                a.children[0].style.width = getComputedStyle(a, '').width
            }, false);
        }
    })()
	}
	
	$('.header-dropdown__nav ul').on('mouseover', 'li', function(){
		
		var $this = $(this);
		var childUl = $this.children('.list-second__down');
  		  
		if(childUl.length > 0){
			$this.parents('.header-dropdown__nav').css('minHeight', childUl.outerHeight() - 40);
		}else{
			$this.parents('.header-dropdown__nav').css('minHeight', 'inherit');
		}	

	});
	
	$('.header-dropdown__nav ul').on('mouseout', 'li', function(){
		var $this = $(this);
		$this.parents('.header-dropdown__nav').css('minHeight', 'inherit');
	});
	
	
    var container = $('#collections-items-wrap');
    
	if(container.length > 0){
		
    	container.imagesLoaded(function () {
    	    container.masonry({
    	        columnWidth: '.collections-item',
    	        gutter: '.collections-item-gutter-sizer',
    	        itemSelector: '.collections-item',
    	        percentPosition: true,
    	        initLayout: false,
    	        isAnimated: true,
    	        hiddenStyle: { overflow: 'hidden' }
    	    });
	
    	    container.masonry('on', 'layoutComplete', function () {
    	        container.addClass('collections-items-wrap-init');
    	    });
			
			container.masonry();
			
    	});
	}
	
    
    $('.map-grid__filter').on('click', 'a', function(e){
       e.preventDefault();
        var $this = $(this);
        var href = $(this).attr('href');
        
        $this.parents('.map-grid__filter').find('a').removeClass('selected');
        $this.addClass('selected');
        
        if(href.length > 0){
            $('.map-grid__floor-part').removeClass('selected');
            $('.map-grid__floor-part' + href).addClass('selected');
        }
    });
	
	
	function getOffset(element)
	{
    	var bound = element.getBoundingClientRect();
    	var html = document.documentElement;

    	return {
    	    top: bound.top + window.pageYOffset - html.clientTop,
    	    left: bound.left + window.pageXOffset - html.clientLeft
    	};
	}
    
    $('.map-grid').on('mouseover', '.map-grid__room', function(e){
        var $this = $(this);
		var offset = getOffset($this[0]);
        var x = offset.left - $('.map-grid').offset().left + ($this[0].getBoundingClientRect().width / 2);
        var y = offset.top - $('.map-grid').offset().top + ($this[0].getBoundingClientRect().height / 2);
		
        var title = $this.data('title');
        var room = $this.data('room');
        var popupHeight,
			popupWidth;
        
        $('.map-grid__room').removeClass('selected');

        $this.addClass('selected');

        if(title){
            $('.map-grid__r-title').addClass('show').text(title);
            popupHeight = $('.map-grid__r-title').innerHeight();
			popupWidth = $('.map-grid__r-title').innerWidth();
            $('.map-grid__r-title').css({
                'top': y - popupHeight - 12,
                'left': x - (popupWidth / 2)
            });
			
            $('.map-grid__r-title').data('room', room);
        }
    });
	
	$('.map-grid').on('mouseout', '.map-grid__room', function(e){
		var $this = $(this);
		$this.removeClass('selected');
        $('.map-grid__r-title').removeClass('show');
	});
    
    $('.map-grid__text').on('click', 'ul li a', function(e){
        e.preventDefault();
        var $this = $(this);
        var href = $(this).attr('href');
        
        $this.parents('.map-grid__text').find('a').removeClass('selected');
        
        $this.addClass('selected');
        
        if(href.length > 0){
            $('.map-grid__r-title').removeClass('show');
            $('.map-grid__floor').removeClass('selected');
            $('.map-grid__floor' + href).addClass('selected');
            $('.map-grid__floor-part, .map-grid__filter ul li, .map-grid__filter ul li a').removeClass('selected');
            $('.map-grid__filter ul li[data-floor="' + href + '"]').addClass('selected');
            $('.map-grid__filter ul li:visible').eq(0).find('a').trigger('click');   
        }
        
    });
    
    $('.map-grid').on('click', '.map-grid__r-title', function(e){
        var $this = $(this);
        var room = $this.data('room');
        
        $this.removeClass('show');
        $('.map-grid__room').removeClass('selected');
    });

    $('.info-block__close').on('click', function (e) {
		e.preventDefault();
        $(this).parents('.info-block').eq(0).stop().fadeOut();
        $.cookie('infoHide', '1', { expires: 7 });
    });
    
    $('.main-slider').owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 3000,
        dots: false,
        nav: true,
        loop: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplayHoverPause:true,
        navText: ['',''],
        navContainer: $('.main-slider-arrows-wrap'),
    });
    
    
    $('.show-main .events-slider').owlCarousel({
        margin: 30,
        items: 3,
        dots: false,
        nav: true,
        autoHeight:true,
        navContainer: $('.show-main .slider-arrows-wrap'),
        responsive : {
           0 : {
               items: 1
           },
           767 : {
               items: 2
           },
           992 : {
               items: 2
           },
           1200 : {
               items: 3
           },
       }
    });
    
    $('.news-main .events-slider').owlCarousel({
        smartSpeed: 300,
        margin: 30,
        items: 4,
        dots: false,
        nav: true,
        navContainer: $('.news-main .slider-arrows-wrap'),
        navText: ['',''],
        responsive : {
           0 : {
               items: 1
           },
           575 : {
               items: 2
           },
           767 : {
               items: 2
           },
           992 : {
               items: 3
           },
           1200 : {
               items: 4
           },
       }
    });
    
    $('.gallery-one-slider').owlCarousel({
        items: 1,
        autoHeight:true,
        dots: false,
        nav: true,
        navText: ['',''],
    });
    
    $('.slider-filials').owlCarousel({
        items: 1,
        autoHeight:true,
        nav: true,
        dots: false,
        navText: ['',''],
    });
    
    $('.product-slider').owlCarousel({
        items: 1,
        autoHeight:true,
        nav: true,
        dots: false,
        navText: ['',''],
    });
    
    $('.slider-action').owlCarousel({
        items: 1,
        autoHeight:true,
        nav: true,
        dots: false,
        navText: ['',''],
    });
    
    $('.slider-other').owlCarousel({
       items: 4,
       autoHeight:true,
       margin: 30,
       dots: false,
       nav: true,
       navContainer: $('.slider-other').parents('.other-news').find('.slider-arrows-wrap'),
       navText: ['',''],
       responsive : {
           0 : {
               items: 1
           },
           767 : {
               items: 2
           },
           992 : {
               items: 2
           },
           1200 : {
               items: 3
           },
           1400 : {
               items: 4
           }
       }
    });
    
    $('.slider-exibitions').owlCarousel({
       items: 3,
       autoHeight:true,
       nav: true,
       dots: false,
       navText: ['',''],
       navContainer: $('.slider-exibitions').parents('.other-news').find('.slider-arrows-wrap'),
       margin: 30,
       responsive : {
           0 : {
               items: 1
           },
           992 : {
               items: 2
           },
           1200 : {
               items: 3
           },
       }
    });
    
     $('.sliderfilial').owlCarousel({
        items: 2,
        autoHeight:true,
		loop: true,
        dots: false,
        nav: true,
        navText: ['',''],
        navContainer: $('.sliderfilial').parents('.other-news').find('.slider-arrows-wrap'),
        margin: 30,
		autoplay: true,
        autoplayTimeout: 3000,
        responsive : {
           0 : {
               items: 1
           },
           992 : {
               items: 2
           },
           1200 : {
               items: 2
           },
       }
    });
    
    $('[data-fancybox]').fancybox({
		animationEffect: 'fade',
        btnTpl: {
            
            download:
              '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="Скачать" href="javascript:;">' +
              '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.62 17.09V19H5.38v-1.91zm-2.97-6.96L17 11.45l-5 4.87-5-4.87 1.36-1.32 2.68 2.64V5h1.92v7.77z"/></svg>' +
              "</a>",
        
            zoom:
              '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="Увеличить">' +
              '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.7 17.3l-3-3a5.9 5.9 0 0 0-.6-7.6 5.9 5.9 0 0 0-8.4 0 5.9 5.9 0 0 0 0 8.4 5.9 5.9 0 0 0 7.7.7l3 3a1 1 0 0 0 1.3 0c.4-.5.4-1 0-1.5zM8.1 13.8a4 4 0 0 1 0-5.7 4 4 0 0 1 5.7 0 4 4 0 0 1 0 5.7 4 4 0 0 1-5.7 0z"/></svg>' +
              "</button>",
            
            slideShow: '<button data-fancybox-play="" class="fancybox-button fancybox-button--play" title="Слайдшоу"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 5.4v13.2l11-6.6z"></path></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M8.33 5.75h2.2v12.5h-2.2V5.75zm5.15 0h2.2v12.5h-2.2V5.75z"></path></svg></button>',
            
            thumbs: '<button data-fancybox-thumbs="" class="fancybox-button fancybox-button--thumbs" title="Миниатюры"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14.59 14.59h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76H5.65V5.65z"></path></svg></button>',
            
            close:
              '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Закрыть">' +
              '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"/></svg>' +
              "</button>",

            arrowLeft:
              '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="Назад">' +
              '<div></div>' +
              "</button>",
        
            arrowRight:
              '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="Далее">' +
              '<div></div>' +
              "</button>",
            
            smallBtn:
                '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="Закрыть">' +
                '<svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg>' +
                "</button>"
        },
	});
    
    
    $("#menu").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
    
     $(document).on("click", ".header-menu-bottom li a[href='#']", function (event) {
        event.preventDefault();
    });
    
    $i = 0;
    $('.menu-toggle-cont').click(function () {
        if ($i == 0) {
            $(this).addClass('menu-toggle-cont_active');
            $('.fixed-menu').slideDown('fast');
            $i = 1;
        } else {
            $(this).removeClass('menu-toggle-cont_active');
            $('.fixed-menu').slideUp('fast');
            $i = 0;
        }
    });
	
    $('.time-menu').each(function () {
        var $list = $(this),
                $listItemSub = $list.find('.sub-menu');
        $listItemSub.each(function () {
            var $el = $(this),
                    $link = $el.find('.time-menu__link'),
                    $subMenu = $el.find('.time-menu__dropdown');
            $link.on('click', function (e) {
                e.preventDefault();
                $(this).toggleClass('open');
                $subMenu.slideToggle()
            });
        });
    });
	
    $('.nav-razdel[data-toggle=dropdown]').off('click.bs.dropdown').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        var preventClass = false;
        var clickedLink = $(this);
        if ($(this).closest('li').hasClass('show')) {
            preventClass = true;
        }

        if (!preventClass) {
            clickedLink.closest('ul').find('li').each(function () {
                $(this).removeClass('show');
                $(this).find('ul').removeClass('show');
            });
            clickedLink.closest('li').addClass('show');
            clickedLink.closest('li').find('ul').addClass('show');
        } else {
            clickedLink.closest('li').removeClass('show');
            clickedLink.closest('li').find('ul').removeClass('show');
        }
    });
    
    var $scrollBtn = $('#scroll-to-top');

    if ($(window).scrollTop() > $('.header').innerHeight()) {
        $scrollBtn.addClass('active');
    }

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > $('.header').innerHeight()) {
            $scrollBtn.addClass('active');
        } else {
            $scrollBtn.removeClass('active');
        }
    });

    $scrollBtn.on('click', function (event) {
        event.preventDefault();
        var btn = $(this);

        $('body, html').animate({
            scrollTop: 0,
        }, {
            duration: 500,
            //easing: 'easeOutQuad',
            complete: function () {
                btn.blur();
            }
        });
    });
    
    $(document).on('click', '.share', function(e){
        e.preventDefault();
        $(this).next().toggleClass('show');
    });
    
    
    colletionAnim = new WOW({
        boxClass:     'collections-item',
        animateClass: 'animated fadeInUp slow', 
        offset:       50,          
        mobile:       true,       
        live:         false        
    });
    
    colletionAnim.init();
    
    historyAnim = new WOW({
        boxClass:     'history-period',
        animateClass: 'animated fadeInUp slow', 
        offset:       50,          
        mobile:       true,       
        live:         false        
    });
	
	historyAnim.init();
	
	mainText = new WOW({
        boxClass:     'text-block-main__text',
        animateClass: 'animated fadeInUp slow', 
        offset:       150,          
        mobile:       true,       
        live:         false        
    });
	
	mainTitle = new WOW({
        boxClass:     'text-block-main__title',
        animateClass: 'animated fadeInUp slow', 
        offset:       150,
		delay: 23,
        mobile:       true,       
        live:         false        
    });
	
	mainButton = new WOW({
        boxClass:     'text-block-main__btn-wrap',
        animateClass: 'animated fadeInUp slow', 
        offset:       100,
        mobile:       true,       
        live:         false        
    });

	mainText.init();
	mainButton.init();
	mainTitle.init();
	
	/* SCROLL HEADER */
	
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('.header').outerHeight() + $('.info-block').outerHeight();
	
	$(window).scroll(function(event){
			hasScrolled();
	});
	
	function hasScrolled() {
		var st = $(this).scrollTop();
		
		if(Math.abs(lastScrollTop - st) <= delta)
			return;
		
		if(st > navbarHeight){
		if (st > lastScrollTop){
			$('.header').removeClass('header--show').addClass('header--hide');
		} else {
			if(st + $(window).height() < $(document).height()) {
				$('.header').addClass("sticky_header");
				$('.header').removeClass('header--hide').addClass('header--show');
			}
		}
		}else{
			$('.header').removeClass("sticky_header");
			$('.header').removeClass('header--hide');
		}
		
		lastScrollTop = st;
	}
	
	$('#accordion').on('shown.bs.collapse', function (e) {
		var targetId = e.target.id,
			header = $('.header'),
			headerHeight = header.outerHeight(),
			scrollEl = $('#' + targetId).prev(),
			top = scrollEl.offset().top,
			scrollTop = 0;
		if(header.css("visibility") == "visibility"){
			console.log(1);
		}else{
			console.log(0);
		}
		
		scrollTop = top - headerHeight;
		
		$('body, html').animate({scrollTop: scrollTop}, 1200);
	})
	
});